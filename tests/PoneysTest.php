<?php
  require_once 'src/Poneys.php';


  class PoneysTest extends \PHPUnit_Framework_TestCase {
	/**
     * @dataProvider additionProvider
     */
    public function test_removePoneyFromField($a,$expected) {
      // Setup
      $Poneys = new Poneys();
        

      // Action
      $Poneys->removePoneyFromField($a);
      
      
      // Assert
      $this->assertEquals($expected,$Poneys->getCount() );

    }
     public function test_addPoneyFromField() {
      // Setup
      $Poneys = new Poneys();
        

      // Action
      $Poneys->addPoneyFromField(1);
      
      // Assert
      $this->assertEquals(9, $Poneys->getCount());

    }



public function additionProvider()
    {
        return array(
          array(3, 5),
          array(2, 6),
          array(1, 7),
          array(4, 4)
        );
    }
 /*  public function test_getName()
    {
$stub = $this->getMockBuilder('Poneys')
             ->getMock();
$list=array('p1', 'p2');

$stub->method('getName')->willReturn($list);
$this->assertEquals(array('p1','p2'), $stub->getName());
}*/


public function test_true() {
      // Setup
      $Poneys = new Poneys();
      

      // Action
     
      
      
      // Assert
      $this->assertTrue($Poneys->placeDispo(14));
      $this->assertFalse($Poneys->placeDispo(16));
     

  }
}
 ?>
